const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const courseSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'Name is required'],
    },
    description: {
      type: String,
      required: [true, 'Description is required'],
    },
    price: {
      type: Number,
      required: [true, 'Price is required'],
    },
    slots: {
      type: Number,
      required: [true, 'Slot is required'],
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    enrollees: [
      {
        userId: {
          type: String,
          required: [true, 'UserId is required'],
        },
        dateEnrolled: {
          type: Date,
          default: new Date(),
        },
      },
    ],
  },
  { timestamps: true }
);

module.exports = mongoose.model('Course', courseSchema);
