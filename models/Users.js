const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  firstName: {
    type: String,
    required: [true, 'First name is required'],
  },
  lastName: {
    type: String,
    required: [true, 'Last name is required'],
  },
  email: {
    type: String,
    required: [true, 'Email is required'],
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
    required: [true, 'Mobile No is required'],
  },
  // The "enrollments" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
  enrollments: [
    {
      courseId: {
        type: String,
        required: [true, 'Course ID is required'],
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: 'Enrolled',
      },
    },
  ],
});

module.exports = mongoose.model('User', userSchema);

// const mongoose = require('mongoose');
// const Schema = mongoose.Schema;

// const userSchema = new Schema(
//   {
//     UserId: {
//       type: String,
//       required: [true, 'User ID is required'],
//     },
//     firstname: {
//       type: String,
//       required: [true, 'First name is required'],
//     },
//     lastname: {
//       type: String,
//       required: [true, 'Last name is required'],
//     },
//     email: {
//       type: String,
//       required: [true, 'Email is required'],
//     },
//     password: {
//       type: String,
//       required: [true, 'Password is required'],
//     },
//     isAdmin: {
//       type: Boolean,
//       default: false,
//     },
//     mobileNumber: {
//       type: String,
//       required: [true, 'Mobile number is required'],
//     },
//     enrollments: [
//       {
//         courseId: {
//           type: String,
//           required: [true, 'Course ID is required'],
//         },
//         dateEnrolled: {
//           date: Date,
//           default: new Date(),
//         },
//         status: {
//           type: String,
//           default: 'Not Enroll',
//         },
//       },
//     ],
//     lastLogin: {
//       type: date,
//       default: new Date(),
//     },
//   },
//   { timestamps: true }
// );

// module.exports = mongoose.model('Course', userSchema);
