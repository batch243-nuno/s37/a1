const Course = require('../models/Courses');
const auth = require('../auth');
const { response } = require('express');

module.exports.addCourse = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const { name, description, price, slots } = request.body;

  let newCourse = new Course({
    name,
    description,
    price,
    slots,
  });

  if (userData.isAdmin) {
    newCourse
      .save()
      .then((result) => {
        console.log(result);
        response.send(true);
      })
      .catch((error) => {
        console.log(error);
        response.send(true);
      });
  } else {
    return response.send('You are not allowed to add Course!');
  }
};

// Retrieve all active courses
module.exports.getAllActive = (request, response) => {
  return Course.find({ isActive: true })
    .then((result) => {
      response.send(result);
    })
    .catch((error) => {
      response.send(error);
    });
};

// Retrieving specific course
module.exports.getCourse = (request, response) => {
  const { courseID } = request.params;

  return Course.findById(courseID)
    .then((result) => {
      response.send(result);
    })
    .catch((error) => {
      response.send(error);
    });
};

// update course
module.exports.updateCourse = (request, response) => {
  const { name, description, price, slots } = request.body;
  const { courseID } = request.params;
  console.log(courseID);
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  let updatedCourse = {
    name,
    description,
    price,
    slots,
  };

  if (userData.isAdmin) {
    Course.findByIdAndUpdate(courseID, updatedCourse, { new: true }).then(
      (result) => {
        response.send(result);
      }
    );
  } else {
    response.send(`You are not allowed to update Courses!`);
  }
};

module.exports.updateStatus = (request, response) => {
  const { isActive } = request.body;
  const { courseID } = request.params;
  const userData = auth.decode(request.headers.authorization);

  let updatedStatus = {
    isActive,
  };

  if (userData.isAdmin) {
    Course.findByIdAndUpdate(courseID, updatedStatus, { new: true })
      .then((result) => {
        response.send(true);
      })
      .catch((error) => {
        console.log(error);
        response.send(false);
      });
  } else {
    response.send(`You are not allowed to update Courses!`);
  }
};

// retrieve all courses
module.exports.getAllCourses = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin) {
    return Course.find({})
      .then((result) => response.send(result))
      .catch((error) => {
        console.log(error);
        response.send(error);
      });
  } else {
    response.send(`Sorry, you are not allowed in this page!`);
  }
};
