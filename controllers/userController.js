const User = require('../models/Users');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Courses = require('../models/Courses');
const Users = require('../models/Users');

module.exports.checkEmailExists = (request, response, next) => {
  return User.find({ email: request.body.email }).then((result) => {
    let message = ``;
    if (result.length > 0) {
      message = `The ${request.body.email} is already taken, please use other email.`;
      return response.send(message);
    } else {
      next();
    }
  });
};

module.exports.registerUser = async (request, resposne) => {
  const { firstName, lastName, email, password, mobileNo } = request.body;
  let newUser = new User({
    firstName,
    lastName,
    email,
    //bcrypt.hashSync(<variable> , <salt rounds>)
    password: bcrypt.hashSync(password, 10),
    mobileNo,
  });
  return newUser
    .save()
    .then((user) => {
      console.log(user);
      resposne.send(
        `Congratulations, Sir/Ma'am ${newUser.firstName}!. You are now registered.`
      );
    })
    .catch((error) => {
      console.log(error);
      response.send(
        `Sorry ${newUser.firstName}, there was an error during the registration. Please try again.`
      );
    });
};

module.exports.loginUser = async (request, response) => {
  const { email, password } = request.body;
  return (result = await User.findOne({ email: email }).then((result) => {
    console.log(result);
    if (result === null) {
      response.send(
        `Your email: ${email} is not yet registered. Registered first!`
      );
    } else {
      const isPasswordCorrect = bcrypt.compareSync(password, result.password);
      if (isPasswordCorrect) {
        return response.send({ accessToken: auth.createAccessToken(result) });
      } else {
        return response.send(`Incorrect password, please try again!`);
      }
    }
  }));
};

module.exports.getProfile = async (request, response) => {
  const { _id } = request.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return response.status(404).send(`User's ID does not exist`);
  }
  return (result = await User.findOne({ _id: _id }).then((result) => {
    if (result === null) {
      response.status(404).send(`User's ID does not exist`);
    } else {
      result.password = '';
      response.status(200).send(result);
    }
  }));
};

module.exports.profileDetails = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  return User.findById(userData.id)
    .then((result) => {
      result.password = 'Confidential';
      return response.send(result);
    })
    .catch((err) => {
      return response.send(err);
    });
};

// update user role
module.exports.updateRole = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const { userIdToUpdate } = request.params;

  if (userData.isAdmin) {
    return User.findById(userIdToUpdate)
      .then((result) => {
        result.isAdmin = !result.isAdmin;
        return User.findByIdAndUpdate(userIdToUpdate, result, {
          new: true,
        })
          .then((document) => {
            document.password = '';
            response.send(document);
          })
          .catch((error) => response.send(error));
      })
      .catch((error) => response.send(error));
  } else {
    response.send(`You don't have access on this page!`);
  }
};

// Enrollment
module.exports.enroll = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const { courseId } = request.params;

  if (!userData.isAdmin) {
    let data = {
      courseId,
      userId: userData.id,
    };
    // const foundMacth = await Courses.findById(courseId).then((result) => {
    //   console.log(data.userId);
    //   console.log(result.enrollees);

    //   const idArray = result.enrollees.map((item) => {
    //     if (item.userId === data.userId) {
    //       console.log(`You are already enrolled in this Course`);
    //     } else {
    const isCourseUpdated = Courses.findById(courseId)
      .then((result) => {
        result.enrollees.push({
          userId: data.userId,
        });
        result.slots -= 1;

        return result
          .save()
          .then((success) => true)
          .catch((error) => response.send(false));
      })
      .catch((error) => {
        console.log(error);
        return response.send(false);
      })
      .catch((error) => console.log(error));

    const isUserUpdated = Users.findById(userData.id).then((result) => {
      result.enrollments.push({
        courseId: data.courseId,
      });
      return result
        .save()
        .then((success) => true)
        .catch((error) => false);
    });

    isUserUpdated && isCourseUpdated
      ? response.send(`You are now enrolled to this course`)
      : response.send(
          `We encounter an error in your enrollment, please try again!`
        );
    //   }
    // });
    // });
  } else {
    response.send(`You are admin, you cannot enroll to course`);
  }
};

// module.exports.checkEmailExists = async (request, response) => {
//   const { email } = request.body;
//   try {
//     const user = await User.find({ email });

//     if (user.length > 0) {
//       return response.send(
//         `The ${email} is alresy taken, please use other email.`
//       );
//     } else {
//       return response
//         .status(200)
//         .send(`That the email: ${email} is not yet taken.`);
//     }
//   } catch (error) {
//     response.status(400).send(`error: ${error.message}`);
//   }
// };

// module.exports = {
//   checkEmailExists,
// };
