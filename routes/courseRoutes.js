const express = require('express');
const router = express.Router();

const courseController = require('../controllers/courseController');
const auth = require('../auth');

// routes
// add course
router.post('/', auth.verify, courseController.addCourse);

// display all course
router.get('/allCourses', auth.verify, courseController.getAllCourses);

// display all active course
router.get('/allActiveCourses', courseController.getAllActive);

// display specific course
router.get('/:courseID', courseController.getCourse);

// update a course
router.put(
  '/updateCourse/:courseID',
  auth.verify,
  courseController.updateCourse
);

// update isActive property
router.patch('/:courseID/archive/', auth.verify, courseController.updateStatus);

module.exports = router;
