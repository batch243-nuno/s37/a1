const express = require('express');
const router = express.Router();
// const auth = require

const userController = require('../controllers/userController');
const auth = require('../auth');

router.post('/checkEmail', userController.checkEmailExists);

router.post(
  '/register',
  userController.checkEmailExists,
  userController.registerUser
);

router.post('/login', userController.loginUser);

router.post('/details', auth.verify, userController.getProfile);

router.get('/profile', userController.profileDetails);

router.patch('/updateRole/:userIdToUpdate', userController.updateRole);

router.post('/enroll/:courseId', auth.verify, userController.enroll);

module.exports = router;
